from flask_frozen import Freezer
from flask_flatpages import FlatPages

import app

freezer = Freezer(app.app)

pages = FlatPages(app.app)


if __name__ == '__main__':
	@freezer.register_generator
	def pages_urls():
		for p in pages:
			yield "page",{'path':p.path}
	
        freezer.freeze()
