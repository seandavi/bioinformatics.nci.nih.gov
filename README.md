## Overview
This is a little home page I sketched out using [Twitter Bootstrap](http://twitter.github.com/bootstrap/ ) as the CSS/javascript framework.

## Usage
1.  git checkout
2.  cd into site
3.  Open index.html
4.  Edit to your heart's content
5.  git add 
6.  git commit
7.  push results back to bitbucket
