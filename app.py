from flask import Flask, url_for
from flask import render_template
from flask_flatpages import FlatPages
from Bio import Entrez, Medline
import os
import datetime

from bioinformaticsncinihgov.software import SoftwareList

app = Flask(__name__)

# set up flatpages
app.config.FLATPAGES_ROOT = 'pages'
pages = FlatPages(app)

app.debug = True


@app.route("/")
def hello():
    return render_template('home.html')

@app.route('/software/')
def software_list():
    software = SoftwareList(os.path.join(os.path.dirname(__file__),'software.yaml'))
    return render_template('software.html',software = software.software)

@app.route('/publications/')
def publication_list():
    pubids = set()
    with open('pubmed_list.txt','r') as pubmed_file:
        for line in pubmed_file:
            pubids.add(line.strip())
    handle = Entrez.efetch(db="pubmed",
                           id = pubids,
                           rettype = "medline",
                           retmode = "text",
                           email = 'sdavis2@mail.nih.gov')
    records = []
    for rec in Medline.parse(handle):
        rec['DATE']=datetime.datetime.strptime(rec['EDAT'].split(" ")[0],'%Y/%m/%d')
        rec['YEAR']=rec['DATE'].year
        records.append(rec)
    return render_template('publication_list.html',records=records)

@app.route('/<path:path>/')
def page(path):
    page = pages.get_or_404(path)
    template = page.meta.get('template', 'index2.html')
    return render_template(template, page=page)

if __name__ == "__main__":
    app.run()

