$(document).ready(function() {

    function fillFeed(url,selector) {
	var items=[];
	console.log(url);
	console.log(selector);

	$.ajax({url:url,
		success:function(xml) {
		    $(xml).find('item').each(function() {
			var $this = $(this),
			item = {
			    title: $this.find("title").text(),
			    link: $this.find("link").text(),
			    description: $this.find("description").text(),
			    pubDate: $this.find("pubDate").text(),
			    author: $this.find("author").text()
			};
			items.push(item);
		    });
		    for(n = 0; n < Math.min(items.length,5); n++) {
			var item = items[n];
			selector.append('<li><a href="' + 
					item.link + '">' + item.title + '</a></li>');
		    }
		    
		},
		async:false})}
    fillFeed('/static/_feeds/biostars.rss',$('#forumposts ul'));
    fillFeed('/static/_feeds/bioinformaticstwitter.rss',$('#bioinformaticstwitter ul'));
    fillFeed('/static/_feeds/eventsfeed.rss',$('#eventsfeed ul'));
});
